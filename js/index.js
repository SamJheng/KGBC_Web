$(window).on('load', function () {
      // console.log("Window Loaded");
      // $(".loading-page").hide();
      // alert("頁面載入完成");
 });

$(function(){
	// nav bar效果
	$(window).scroll(function(evt){
	  if($(window).scrollTop()>0){
	    $(".navbar").removeClass("navbar_js-top");
	  }else{
	    $(".navbar").addClass("navbar_js-top");
	  }
	});
	var shadow=$(".lightbox");
		shadowImg=$(".lightbox").children("img");
	shadow.each(function(){
		var _this=$(this),
			boxClose=$(this).children(".lightbox__close-svg");
			boxClose.on('click',function(e){
				e.stopPropagation();
				_this.removeClass("lightbox_show");
			});
	});
	$(".lightbox_img").each(function(){
        $(this).on('click',function(e){
        	e.stopPropagation();
            var thisImg=$(this).children("img").attr("src");
            // console.log(img);
            shadow.addClass("lightbox_show");
            shadowImg.attr("src",thisImg);
        })
    });
	// gallery_grid特效
    $(window).scroll(function() {
		// console.log("window:"+$(window).scrollTop());
		// console.log("gallery_grid:"+$(".gallery_grid-layout").offset().top);
		$(".gallery_grid-layout__item").each(function() {
			var winTop= $(window).scrollTop(),
				gridTop=$(this).offset().top,
				fadeItem=$(this);
			if (winTop >= gridTop-800) {
				fadeItem.addClass("gallery_grid-layout__item_fadein");
			} else {
				fadeItem.removeClass("gallery_grid-layout__item_fadein");
			}
		});
		$(".ministry-grid__item").each(function() {
			var winTop= $(window).scrollTop(),
				gridTop=$(this).offset().top,
				fadeItem=$(this);
			if (winTop >= gridTop-500) {
				fadeItem.addClass("ministry-grid__item_fadein");
			} else {
				fadeItem.removeClass("ministry-grid__item_fadein");
			}
		});
		$(".block-door").each(function() {
			var winTop= $(window).scrollTop(),
				gridTop=$(this).offset().top,
				fadeItem=$(this);
			if (winTop >= gridTop-300) {
				fadeItem.addClass("block-door_close");
			} else {
				fadeItem.removeClass("block-door_close");
			}
		});
	});
	$(".navbar").each(function(){
		var _this=$(this),
			mobileBtn=_this.find(".navbar_mobile"),
			mobileNavbar=_this.find(".mobile-navbar");
		mobileBtn.on('click',function(e){
        	e.stopPropagation();
        	_this.toggleClass("mobile_active");
        });
	});
});
